<?php
use App\Benchmark;

require_once __DIR__ . '/vendor/autoload.php';

$benchmark = new Benchmark;
echo sprintf( "Creation average time (ms): %s\n", $benchmark->measureCreation( 10 ) );
echo sprintf( "Updating average time (ms): %s\n", $benchmark->measureUpdating( 10 ) );