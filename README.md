# ТЗ

У CARDPR есть API. Задача написать программу/тест:
  1. Проверит работу метода https://core.codepr.ru/api/v2/crm/user_create_or_update;
  1. Проведет замеры времени ответа API на запросы;
  1. Проведет нагрузочное тестирование.

  * Сайт https://ru.cardpr.com/
  * Документация Api http://help.cardpr.com/ru/articles/2680785
  * Тестовый аккаунт testphp.codepr.ru
  * Ключ 5240f691-60b0-4360-ac1f-601117c5408f
  * Расчетное время выполнения – не более 1 часа.
  * Если какая-то информация в тестовом задании не указана, вы можете выполнить на свое усмотрение.
  
# Консоль

Аргументы:
  * tests. Запуск тестов в папке tests
  * benchmark. Запуск измерения скорости отклика API
  * install. Установка необходимых модулей для запуска остальных команд
  
Пример:
```bash
sudo ./console tests
```

# Развертывание

Выполнить в консоли:
```bash
cd $(eval echo ~$USER) && curl https://bitbucket.org/brocoder_team/code/downloads/console.sh -L -o ./console.sh && sudo chmod +x ./console.sh && sudo ./console.sh install
```

Тестировалось на чистой Ubuntu 18.04.03 (LTS) x64