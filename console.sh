#!/usr/bin/env bash

if [[ $1 = 'install' ]]
then
    mkdir -p /var/www/html
    chown -R $USER:$USER /var/www/html
    cd /var/www/html
    
    apt-get install git
    git clone https://brocoder1@bitbucket.org/brocoder_team/code.git .
    chmod +x ./console.sh

    apt-get update
    apt -y install software-properties-common
    add-apt-repository ppa:ondrej/php -y
    apt-get update

    apt -y install php7.4
    apt-get install -y php7.4-{mbstring,dom,zip}
    
    apt install composer -y
    composer install
elif [[ $1 = 'tests' ]]
then
    ./vendor/bin/phpunit --testdox ./tests/
elif [[ $1 = 'benchmark' ]]
then
    php ./benchmark.php
else
    echo 'Unknown argument. Available arguments: install, tests, benchmark'
fi