<?php
namespace App\Tests;

use App\APIRequest;
use PHPUnit\Framework\TestCase;

class UserCreateOrUpdateTest extends TestCase
{
    /**
     * При отправке уникальных телефона и емейла должен создаваться новый юзер
     */
    public function testUserCreation()
    {
        $userHashes = [];
        for( $userIdx = 0; $userIdx < 2; ++$userIdx ) {
            $request = new APIRequest;
            $request->setPhone( $this->generatePhone() );
            $request->setEmail( $this->generateEmail() );
            $request->setName( 'John' );
            $execution = $request->execute();

            $this->assertTrue( $execution->isSuccess() );

            $userCreated = $execution->getResponse();
            $userHashes[] = $userCreated[ 'user_hash' ];
        }
        $this->assertNotEquals( $userHashes[ 0 ], $userHashes[ 1 ] );
    }

    /**
     * Если юзер с такой мобилой уже есть в системе, его должно обновить, а не создавать нового
     */
    public function testUserUpdating()
    {
        $userHashes = [];
        $phone = $this->generatePhone();
        $email = $this->generateEmail();
        for( $userIdx = 0; $userIdx < 2; ++$userIdx ) {
            $request = new APIRequest;
            $request->setPhone( $phone );
            $request->setEmail( $email );
            $request->setName( 'John' );
            $execution = $request->execute();

            $this->assertTrue( $execution->isSuccess() );

            $userCreated = $execution->getResponse();
            $userHashes[] = $userCreated[ 'user_hash' ];
        }
        $this->assertEquals( $userHashes[ 0 ], $userHashes[ 1 ] );
    }
    
    public function testRequestShouldProhibitedWithInvalidAppKey()
    {
        $request = new APIRequest;
        $request->setPhone( '+79098887766' );
        $execution = $request->execute();
        $this->assertTrue( $execution->isSuccess() );
        
        $request = new APIRequest;
        $request->setPhone( '+79098887766' );
        $request->setAppkey( '5240f691-60b0-0000-0000-601117c5408f' );
        $execution = $request->execute();
        $this->assertTrue( $execution->isError() );
    }

    public function testAppKeyShouldBeRequired()
    {
        $request = new APIRequest;
        $request->setAppkey( '' );
        $execution = $request->execute();
        $this->assertTrue( $execution->isError() );
        $this->assertEquals( 'app_key not set', $execution->getErrorMessage() );
    }
    
    public function testPhoneShouldBeRequired()
    {
        $request = new APIRequest;
        $execution = $request->execute();
        $this->assertTrue( $execution->isError() );
        $this->assertEquals( 'Телефон не указан', $execution->getErrorMessage() );
    }
    
    public function testNameShouldBeRequired()
    {
        $request = new APIRequest;
        $request->setPhone( $this->generatePhone() );
        $execution = $request->execute();
        $this->assertTrue( $execution->isError() );
        $this->assertEquals( 'Имя не указано', $execution->getErrorMessage() );
    }

    public function testEmailUniquenessAmongAllUsers()
    {
        $email = $this->generateEmail();

        $request = new APIRequest;
        $request->setPhone( $this->generatePhone() );
        $request->setEmail( $email );
        $request->setName( 'John' );
        $execution = $request->execute();
        $this->assertTrue( $execution->isSuccess() );

        $request = new APIRequest;
        $request->setPhone( $this->generatePhone() );
        $request->setEmail( $email );
        $request->setName( 'John' );
        $execution = $request->execute();
        $this->assertTrue( $execution->isError() );
        $this->assertEquals( 'Пользователь с этим email уже существует.', $execution->getErrorMessage() );
    }

    public function testPhoneValidation()
    {
        $phonesInvalid = [ '90000', '+79000000', '+7900000000o' ];
        foreach( $phonesInvalid as $phoneInvalid ) {
            $request = new APIRequest;
            $request->setPhone( $phoneInvalid );
            $request->setEmail( $this->generateEmail() );
            $request->setName( 'John' );
            $execution = $request->execute();
            
            $this->assertTrue( $execution->isError() );            
            $this->assertEquals( 'Неверный формат телефона', $execution->getErrorMessage() );
        }
    }

    public function testBirthdayValidation()
    {
        $birthdaysInvalid = [ 'dd.mm.yy', '12.12.0000', '12.12.2050', '-1.12.2050' ];
        foreach( $birthdaysInvalid as $birthdayInvalid ) {
            $request = new APIRequest;
            $request->setPhone( $this->generatePhone() );
            $request->setEmail( $this->generateEmail() );
            $request->setName( 'John' );
            $request->setBirthday( $birthdayInvalid );
            $execution = $request->execute();
            
            $this->assertTrue( $execution->isError() );
            $this->assertEquals( 'Не верный формат даты', $execution->getErrorMessage() );
        }
    }

    private function generatePhone(): string
    {
        return '+79' . mt_rand( 1, 999999999 );
    }

    private function generateEmail(): string
    {
        return sprintf( 'email%s@domain%s.com', mt_rand( 1, 999999999999 ), mt_rand( 1, 999999999999 ) );
    }
}