<?php
namespace App;

class Benchmark
{
    /**
     * Вернет среднее количество милисекунд, затраченное на исполнение запросов по созданию
     */
    public function measureCreation( int $requests ): int
    {
        $transferTimes = [];
        for( $i = 0; $i < $requests; ++$i ) {
            $request = new APIRequest;
            $request->setPhone( $this->generatePhone() );
            $request->setEmail( $this->generateEmail() );
            $request->setName( 'John' );
            $execution = $request->execute();
            $transferTimes[] = $execution->getTransferTimeAsSeconds();
        }
        return round( array_sum( $transferTimes ) / count( $transferTimes ), $requests ) * 1000;
    }

    /**
     * Вернет среднее количество милисекунд, затраченное на исполнение запросов по обновлению
     */
    public function measureUpdating( int $requests ): int
    {
        $transferTimes = [];
        $phone = $this->generatePhone();
        $email = $this->generateEmail();
        for( $i = 0; $i < $requests; ++$i ) {
            $request = new APIRequest;
            $request->setPhone( $phone );
            $request->setEmail( $email );
            $request->setName( 'John' );
            $execution = $request->execute();
            $transferTimes[] = $execution->getTransferTimeAsSeconds();
        }
        return round( array_sum( $transferTimes ) / count( $transferTimes ), $requests ) * 1000;
    }

    private function generatePhone(): string
    {
        return '+79' . mt_rand( 1, 999999999 );
    }

    private function generateEmail(): string
    {
        return sprintf( 'email%s@domain%s.com', mt_rand( 1, 999999999999 ), mt_rand( 1, 999999999999 ) );
    }
}