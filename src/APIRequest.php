<?php
namespace App;

use GuzzleHttp\Client;
use GuzzleHttp\TransferStats;
use PHPUnit\Util\Exception;

class APIRequest
{
    private CONST APP_KEY = '5240f691-60b0-4360-ac1f-601117c5408f';
    private CONST REQUESTS_TIMEOUT_SEC = 1;

    private string $appkey = 'default';
    private ?string $phone = null;
    private ?string $email = null;
    private ?string $name = null;
    private ?string $surname = null;
    private ?string $middlename = null;
    private ?string $birthday = null;
    private ?string $discount = null;
    private ?string $bonus = null;
    private ?string $balance = null;
    private ?string $customerId = null;
    private ?string $link = null;
    private ?string $sms = null;

    public function setAppkey( string $appkey )
    {
        $this->appkey = $appkey;
    }

    public function setPhone( string $phone )
    {
        $this->phone = $phone;
    }

    public function setEmail( string $email )
    {
        $this->email = $email;
    }

    public function setName( string $name )
    {
        $this->name = $name;
    }

    public function setSurname( string $surname )
    {
        $this->surname = $surname;
    }

    public function setMiddlename( string $middlename )
    {
        $this->middlename = $middlename;
    }

    public function setBirthday( string $birthday )
    {
        $this->birthday = $birthday;
    }

    public function setDiscount( string $discount )
    {
        $this->discount = $discount;
    }

    public function setBonus( string $bonus )
    {
        $this->bonus = $bonus;
    }

    public function setBalance( string $balance )
    {
        $this->balance = $balance;
    }

    public function setCustomerId( string $customerId )
    {
        $this->customerId = $customerId;
    }

    public function setLink( string $link )
    {
        $this->link = $link;
    }

    public function setSms( string $sms )
    {
        $this->sms = $sms;
    }

    public function execute(): APIResponse
    {
        $result = [];
        $client = new Client( [ 'verify' => false, 'http_errors' => false ] );

        $params = [];
        if( $this->appkey === 'default' ) {
            $params[ 'app_key' ] = self::APP_KEY;
        }
        else {
            $params[ 'app_key' ] = $this->appkey;
        }
        if( ! is_null( $this->phone ) ) {
            $params[ 'phone' ] = $this->phone;
        }
        if( ! is_null( $this->email ) ) {
            $params[ 'email' ] = $this->email;
        }
        if( ! is_null( $this->name ) ) {
            $params[ 'name' ] = $this->name;
        }
        if( ! is_null( $this->surname ) ) {
            $params[ 'surname' ] = $this->surname;
        }
        if( ! is_null( $this->middlename ) ) {
            $params[ 'middlename' ] = $this->middlename;
        }
        if( ! is_null( $this->birthday ) ) {
            $params[ 'birthday' ] = $this->birthday;
        }
        if( ! is_null( $this->discount ) ) {
            $params[ 'discount' ] = $this->discount;
        }
        if( ! is_null( $this->bonus ) ) {
            $params[ 'bonus' ] = $this->bonus;
        }
        if( ! is_null( $this->balance ) ) {
            $params[ 'balance' ] = $this->balance;
        }
        if( ! is_null( $this->customerId ) ) {
            $params[ 'customerId' ] = $this->customerId;
        }
        if( ! is_null( $this->link ) ) {
            $params[ 'link' ] = $this->link;
        }
        if( ! is_null( $this->sms ) ) {
            $params[ 'sms' ] = $this->sms;
        }

        $response = $client->request(
            'POST',
            'https://core.codepr.ru/api/v2/crm/user_create_or_update',
            [
                'form_params' => $params,
                'on_stats'    => function( TransferStats $stats ) use ( &$result ) {
                    $result[ 'transfer_time' ] = $stats->getTransferTime();
                }
            ]
        );
        sleep( self::REQUESTS_TIMEOUT_SEC );
        
        return new APIResponse( $response, $result[ 'transfer_time' ] );
    }
}