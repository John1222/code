<?php
namespace App;

use JsonException;
use Psr\Http\Message\ResponseInterface;

class APIResponse
{
    private bool $isError = false;
    private string $errorMessage = '';
    private array $responseAsJson;
    private float $transferTimeSec;
    
    public function __construct( ResponseInterface $response, float $transferTimeSec )
    {
        $contents = $response->getBody()->getContents();
        try {
            $this->responseAsJson = json_decode( $contents, true, 512, JSON_THROW_ON_ERROR );
            $this->transferTimeSec = $transferTimeSec;
        }
        catch( JsonException $e ) {
            throw new APIException( "API returned invalid JSON:\n{$contents}" );
        }

        if( isset( $this->responseAsJson[ 'error' ] ) &&
            preg_match( '/Превышен лимит запросов в секунду/u', $this->responseAsJson[ 'error' ] ) ) {
            throw new APIException( 'Requests speed limit exceeded' );
        }
        
        if( isset( $this->responseAsJson[ 'error' ] ) ) {
            $this->isError = true;
            $this->errorMessage = $this->responseAsJson[ 'error' ];
        }
    }
    
    public function isSuccess(): bool
    {
        return ! $this->isError;
    }
    
    public function isError(): bool
    {
        return $this->isError;
    }
    
    public function getErrorMessage(): string
    {
        return $this->errorMessage;
    }

    public function getTransferTimeAsSeconds(): float
    {
        return $this->transferTimeSec;
    }

    public function getResponse(): array
    {
        return $this->responseAsJson;
    }
}